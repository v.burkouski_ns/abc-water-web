$(window).scroll(function(){
    if($(document).scrollTop() > 0) {
        $(".top_header").hide();
        $(".scrolled_header").show();
    }
    else {
        $(".top_header").show();
        $(".scrolled_header").hide();
    }
});